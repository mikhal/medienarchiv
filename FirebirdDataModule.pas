unit FirebirdDataModule;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client,
  FireDAC.Phys.IBBase;

type
  TDataModuleFirebird = class(TDataModule)
    FirebirdConnection: TFDConnection;
    FBDriverLink: TFDPhysFBDriverLink;
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

//var
//  DataModuleFirebird: TDataModuleFirebird;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TDataModuleFirebird.DataModuleDestroy(Sender: TObject);
begin
  if FirebirdConnection.Connected then
    FirebirdConnection.Connected := False;
end;

end.

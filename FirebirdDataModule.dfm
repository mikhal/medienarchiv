object DataModuleFirebird: TDataModuleFirebird
  OldCreateOrder = False
  OnDestroy = DataModuleDestroy
  Height = 322
  Width = 469
  object FirebirdConnection: TFDConnection
    Params.Strings = (
      'Database=D:\Firebird\MARCHIV.FDB'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'Port=3050'
      'CharacterSet=UTF8'
      'DriverID=FB30')
    Left = 32
    Top = 16
  end
  object FBDriverLink: TFDPhysFBDriverLink
    DriverID = 'FB30'
    Left = 160
    Top = 16
  end
end

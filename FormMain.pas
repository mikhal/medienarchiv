unit FormMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.UITypes, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms,
  Vcl.Dialogs, FirebirdDataModule;

type
  TMainForm = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private-Deklarationen }
    FFormCreate: Boolean;
    FDataModuleFirebird: TDataModuleFirebird;
  public
    { Public-Deklarationen }
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}

procedure TMainForm.FormActivate(Sender: TObject);
begin
  if FFormCreate then
  begin
    FFormCreate := False;
    FDataModuleFirebird := TDataModuleFirebird.Create(self);
    try
      FDataModuleFirebird.FirebirdConnection.Connected := True;
    except
      MessageDlg(Format('FEHLER!%sDie Datenbankverbindung konnte nicht hergestellt werden!%sProgramm wird beendet!', [#13#10#13#10,#13#10]),
                         mtError, [mbCancel], 0);
      self.Close;
    end;
  end
  else
  begin
    //
  end;
end;

procedure TMainForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if FDataModuleFirebird.FirebirdConnection.Connected then

end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  FFormCreate := True;
end;

end.

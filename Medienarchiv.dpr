program Medienarchiv;

uses
  Vcl.Forms,
  FormMain in 'FormMain.pas' {MainForm},
  FirebirdDataModule in 'FirebirdDataModule.pas' {DataModuleFirebird: TDataModule};

{$R *.res}

begin
  ReportMemoryLeaksOnShutdown := True; // Speicherlecks ausweisen, wenn vorhanden
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.
